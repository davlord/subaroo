# Subaroo
A subtitle file manipulation tool

## Usage
### Shift subtitle time
```
python subaroo.py (modifier in milliseconds) (subtitle file)
```

#### Examples

Shift subtitles 2,5s later
```
python subaroo.py +2500 my-subtitles.srt
```

Shift subtitles 2,5s sooner
```
python subaroo.py -2500 my-subtitles.srt
```

## Limitations
* Only support srt subtitle format for now (ask for additional format if needed)
* Do not check/limit "abusive" or wrong shift modifier values
