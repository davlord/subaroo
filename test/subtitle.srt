1
00:00:08,106 --> 00:00:11,456
{\pos(192,235)}- Tu devais pas sortir les poubelles ?
- Je m'en occupe.

2
00:00:16,424 --> 00:00:17,624
Je les prends.

3
00:00:21,700 --> 00:00:23,250
J'allais les prendre.

4
00:00:26,781 --> 00:00:28,508
- Dylan.
- Bonjour, Mme Dunphy.

5
00:00:28,633 --> 00:00:31,983
- J'attends Haley.
- Tu as sonn� ?

6
00:00:32,260 --> 00:00:35,273
J'ai envoy� un texto.
Elle a dit qu'elle arrivait.

7
00:00:35,454 --> 00:00:37,828
- Super. Passe-lui le bonjour.
- D'accord.

8
00:00:39,135 --> 00:00:40,248
D-Money !

9
00:00:40,645 --> 00:00:42,624
Wesh, c'est Dylan le lascar.

10
00:00:42,806 --> 00:00:45,151
- Du "D", au "y", au...
- Bonjour, M. Dunphy.

11
00:00:45,276 --> 00:00:47,852
{\pos(192,210)}Entre. T'arrives juste � temps
pour la fin du match.

12
00:00:47,977 --> 00:00:50,177
{\pos(192,210)}Je suis pas trop fan de base-ball.

13
00:00:50,639 --> 00:00:52,908
- Haley dit bonjour.
- J'�tais ironique.

14
00:00:53,033 --> 00:00:56,305
Je vais te mettre dans le bain.
Assieds-toi. Gare-toi.

15
00:00:57,705 --> 00:00:59,279
Allez, je mords pas.

16
00:01:01,652 --> 00:01:02,652
Je rigole.

17
00:01:03,013 --> 00:01:03,937
Je rigole.

18
00:01:04,118 --> 00:01:07,644
{\pos(192,225)}Bon, tu vois ce joueur ?
Un truc � savoir sur lui.

19
00:01:07,997 --> 00:01:09,979
{\pos(192,225)}Il a jamais rien touch� de sa vie,

20
00:01:10,104 --> 00:01:12,510
{\pos(192,225)}mais je crois
qu'il pense avoir une chance,

21
00:01:12,635 --> 00:01:16,575
{\pos(192,225)}alors que c'est une tr�s mauvaise id�e.
�a va, Haley et toi ?

22
00:01:23,252 --> 00:01:25,943
Je voudrais rester avec toi
et faire voler des avions en jouet.

23
00:01:26,068 --> 00:01:29,463
{\pos(192,225)}Ce sont pas des jouets.
Ce sont des miniatures, tr�s complexes.

24
00:01:29,645 --> 00:01:32,963
{\pos(192,225)}Pour en faire voler,
il faut s'y conna�tre en profil d'aile,

25
00:01:33,088 --> 00:01:36,720
portance, pouss�e et sustentation,
des principes d'a�rodynamique.

26
00:01:37,138 --> 00:01:39,691
C'est �crit "12 ans et plus",
sur la bo�te.

27
00:01:39,816 --> 00:01:40,634
Quoi ?!

28
00:01:41,862 --> 00:01:45,600
{\pos(192,225)}Tu pourras jouer avec Jay plus tard.
Aujourd'hui, tu vois Luke.

29
00:01:45,725 --> 00:01:48,501
{\pos(192,225)}- Pourquoi ?
- Parce que sa m�re t'a invit�.

30
00:01:48,626 --> 00:01:50,580
{\pos(192,225)}Une famille doit rester soud�e.
Hein, Jay ?

31
00:01:50,844 --> 00:01:52,935
�a doit �tre une faute de frappe.

32
00:01:53,060 --> 00:01:55,989
<i>Les hommes ont besoin de loisirs.
Le p�re de Manny en avait beaucoup.</i>

33
00:01:56,498 --> 00:01:58,360
Les randonn�es dans le d�sert,

34
00:01:58,485 --> 00:02:00,911
le ski en se laissant tomber d'un...

35
00:02:01,144 --> 00:02:04,144
- On dit comment...
- Un h�licopt�re.

36
00:02:04,976 --> 00:02:07,922
Une fois, pour un d�fi,
il a m�me box� avec un alligator.

37
00:02:08,047 --> 00:02:09,830
S'est battu. On se bat.

38
00:02:10,502 --> 00:02:13,263
- On peut pas boxer avec un alligator.
- Tu es s�r ?

39
00:02:13,388 --> 00:02:15,760
Comment ils mettent les gants,
avec leurs griffes ?

40
00:02:16,181 --> 00:02:18,581
Ils ont pas
de toutes petites mains ?

41
00:02:19,903 --> 00:02:22,057
- Voil�, j'ai oubli� de quoi on parlait.
- Bref,

42
00:02:22,536 --> 00:02:24,623
un homme a besoin de ses loisirs,

43
00:02:24,748 --> 00:02:26,608
qu'il risque sa vie

44
00:02:26,733 --> 00:02:30,441
ou qu'il fasse voler de petits avions
� une distance raisonnable.

45
00:02:33,875 --> 00:02:36,376
- Attends, tu fais quoi ?
- On va acheter des couches.

46
00:02:36,501 --> 00:02:39,415
- �a sera rapide.
- On est � Lidl.

47
00:02:39,540 --> 00:02:42,587
- Oui, c'est l� qu'on les ach�te.
- Depuis quand ?

48
00:02:42,712 --> 00:02:45,593
Tu te souviens de quand on a adopt�
ce b�b�, il y a quelques mois ?

49
00:02:45,718 --> 00:02:47,218
Depuis ce moment-l�.

50
00:02:49,566 --> 00:02:51,042
Mitchell est snob.

51
00:02:51,655 --> 00:02:53,355
Non, je suis perspicace.

52
00:02:53,589 --> 00:02:56,313
Le terme officiel
qu'utilisent les snobs.

53
00:02:56,730 --> 00:02:59,765
� notre rencontre, il m'a ignor�
parce que j'�tais un p�quenaud

54
00:02:59,890 --> 00:03:03,894
venu d'une ferme dans le Missouri
alors que lui, c'est un rat des villes.

55
00:03:04,145 --> 00:03:07,340
- Qui dit "rat des villes" ?
- Un rat des champs.

56
00:03:10,292 --> 00:03:12,804
Je vais aller attendre
dans la voiture.

57
00:03:13,403 --> 00:03:15,235
T'en profites
pour sortir les poubelles ?

58
00:03:15,533 --> 00:03:17,904
- D�j� fait.
- C'est bon, on l'a d�j� fait.

59
00:03:18,729 --> 00:03:20,889
- Il est un peu nerveux.
- Pas �tonnant.

60
00:03:21,014 --> 00:03:23,872
Un ado a pas envie de sympathiser
avec le p�re de sa copine.

61
00:03:23,997 --> 00:03:26,547
Je croyais
qu'on avait d�pass� tout �a.

62
00:03:27,124 --> 00:03:29,216
Il faut qu'on franchisse un cap.

63
00:03:29,341 --> 00:03:30,341
Vraiment ?

64
00:03:30,586 --> 00:03:32,544
Je pensais
qu'il fallait imposer son style.

65
00:03:32,725 --> 00:03:35,681
Imposer son style,
�a sert justement � franchir un cap.

66
00:03:35,806 --> 00:03:37,049
Tu savais pas ?

67
00:03:37,218 --> 00:03:39,886
On peut pas dire que tu aies franchi
le cap avec mon p�re.

68
00:03:40,143 --> 00:03:43,055
- De quoi tu parles ?
- On est mari�s depuis 16 ans,

69
00:03:43,235 --> 00:03:45,301
et t'es toujours intimid� par lui.

70
00:03:45,426 --> 00:03:46,276
Quoi ?!

71
00:03:46,519 --> 00:03:47,830
Tu plaisantes ?

72
00:03:48,220 --> 00:03:50,321
Jay et moi, on est super potes.

73
00:03:54,485 --> 00:03:56,285
Tu veux tra�ner avec moi ?

74
00:03:56,949 --> 00:03:58,521
J'ai amen� des bi�res...

75
00:04:00,616 --> 00:04:01,816
On n'a qu'�...

76
00:04:03,663 --> 00:04:05,942
Sous-titres de La Fabrique :
Tyno & Kevin

77
00:04:06,067 --> 00:04:08,050
www.seriessub.com
www.sous-titres.eu

78
00:04:17,821 --> 00:04:20,009
Manny a pris son jeu pr�f�r�
pour jouer avec Luke.

79
00:04:20,245 --> 00:04:21,505
�a s'appelle <i>Empire</i>.

80
00:04:21,630 --> 00:04:24,638
Le but, c'est d'�tendre son territoire
et de dominer le monde.

81
00:04:25,531 --> 00:04:27,474
{\pos(192,180)}Luke va adorer.
Il joue dehors.

82
00:04:27,940 --> 00:04:30,248
Je vais faire les boutiques.
Besoin de quelque chose ?

83
00:04:30,373 --> 00:04:32,578
C'est tr�s gentil, mais non. Merci.

84
00:04:33,515 --> 00:04:34,815
Et �a, alors ?

85
00:04:35,049 --> 00:04:37,109
Tu ne dis pas bonjour
� Gloria et Manny ?

86
00:04:37,288 --> 00:04:38,744
Bonjour, Gloria et Manny.

87
00:04:38,869 --> 00:04:42,134
- Bon, maman, et �a, alors ?
- Non, tu vas mettre une robe.

88
00:04:42,259 --> 00:04:43,459
Maman, allez !

89
00:04:43,584 --> 00:04:46,212
{\pos(192,210)}Quoi ? �a va te tuer d'avoir l'air
d'une fille pour une fois ?

90
00:04:46,413 --> 00:04:49,171
{\pos(192,180)}C'est le mariage d'une amie � toi
que je connais m�me pas.

91
00:04:49,296 --> 00:04:52,590
{\pos(192,180)}Pas de discussion.
Tu peux emprunter une robe � Haley.

92
00:04:52,715 --> 00:04:54,765
�a envoie un message affreux...

93
00:04:54,890 --> 00:04:56,299
Que je suis Haley.

94
00:04:56,424 --> 00:04:59,046
Au lieu de me forcer
� porter une robe,

95
00:04:59,171 --> 00:05:01,633
pourquoi tu ne demandes pas
� Luke de porter un pantalon ?

96
00:05:02,972 --> 00:05:03,772
{\pos(192,280)}Quoi ?

97
00:05:04,716 --> 00:05:06,299
Pourquoi il fait �a ?

98
00:05:09,184 --> 00:05:10,992
Ton pantalon ! Allez !

99
00:05:15,944 --> 00:05:17,774
{\pos(192,210)}<i>Jay et moi, on est potes,</i>

100
00:05:17,953 --> 00:05:20,985
mais avec un genre
d'ast�risque invisible.

101
00:05:21,834 --> 00:05:23,933
Il est pas tr�s bavard...

102
00:05:24,832 --> 00:05:26,106
ou tr�s c�lin.

103
00:05:27,503 --> 00:05:30,089
Une fois, il m'a roul� sur le pied.

104
00:05:30,336 --> 00:05:32,821
En toute honn�tet�,
il venait d'arr�ter de fumer.

105
00:05:32,946 --> 00:05:34,846
Mais, en gros, on est potes.

106
00:05:36,868 --> 00:05:40,475
{\pos(192,210)}T'es oblig� de rester plant� l� ?
Tu me mets mal � l'aise.

107
00:05:44,386 --> 00:05:46,178
Bon sang, assieds-toi l�.

108
00:05:46,357 --> 00:05:49,407
{\pos(192,210)}Tiens cette aile comme �a
pendant que j'ajuste �a.

109
00:05:50,924 --> 00:05:52,074
Allez. Assis.

110
00:06:02,683 --> 00:06:03,883
Pas trop fort.

111
00:06:07,702 --> 00:06:11,369
{\pos(192,210)}Viens faire les boutiques avec moi.
Je t'aiderai � trouver ton bonheur.

112
00:06:12,060 --> 00:06:14,790
C'est tr�s gentil de proposer,
mais c'est pas trop son truc.

113
00:06:15,232 --> 00:06:17,125
En fait, �a serait avec plaisir.

114
00:06:17,467 --> 00:06:20,921
Tu vois, tu sais pas quel est mon truc.
T'en as pas la moindre id�e.

115
00:06:21,611 --> 00:06:24,341
{\pos(192,210)}Je sais quel est ton truc.
Ton truc, c'est de provoquer,

116
00:06:24,466 --> 00:06:27,030
{\pos(192,210)}et le truc de ta s�ur,
c'est de s'enfermer dans sa chambre,

117
00:06:27,155 --> 00:06:29,690
{\pos(192,210)}et le truc de ton fr�re...
Oh, le voil�, son truc.

118
00:06:32,543 --> 00:06:33,643
Remonte-le !

119
00:06:35,322 --> 00:06:37,573
<i>Le probl�me entre Jay et moi,</i>

120
00:06:37,774 --> 00:06:39,744
c'est que notre relation est rest�e

121
00:06:39,869 --> 00:06:42,319
� ce stade primitif, comme au d�but.

122
00:06:42,444 --> 00:06:44,894
C'est le vieux gorille
qui prot�ge ses femelles.

123
00:06:45,019 --> 00:06:47,402
Et arrive ce gorille
plus jeune et plus fort,

124
00:06:47,527 --> 00:06:50,484
qui se balance et se tape sur le torse.
Naturellement,

125
00:06:50,609 --> 00:06:54,205
les femelles accourent,
et pr�sentent leur joli derri�re.

126
00:06:54,330 --> 00:06:57,177
Papa singe veut que �a s'arr�te,
mais il peut rien y faire.

127
00:06:57,302 --> 00:06:58,726
C'est la vie.

128
00:06:58,851 --> 00:07:00,551
C'est pas moi, l'ennemi.

129
00:07:01,665 --> 00:07:03,693
L'ennemi, c'est le braconnier.

130
00:07:08,442 --> 00:07:10,792
�a devrait �tre bon.
N'y touche pas.

131
00:07:15,769 --> 00:07:16,569
Bien.

132
00:07:22,036 --> 00:07:24,067
- Tu veux le faire voler ?
- D'accord.

133
00:07:28,409 --> 00:07:30,708
J'ai le dentifrice et le savon.

134
00:07:30,833 --> 00:07:33,402
Super. Reste plus qu'� ouvrir
notre propre magasin.

135
00:07:33,527 --> 00:07:36,212
- On devait prendre que des couches.
- On a un dicton, � la ferme.

136
00:07:36,337 --> 00:07:40,327
Quitte � amener la mule au march�...
Tu l�ves les yeux au ciel.

137
00:07:41,026 --> 00:07:43,571
On s'est connus � une des fameuses
soir�es jeux de Pepper.

138
00:07:43,696 --> 00:07:46,013
Je me souviens de Mitchell
qui levait les yeux au ciel

139
00:07:46,138 --> 00:07:48,853
- quand j'�tais trop enthousiaste.
- Je faisais pas �a !

140
00:07:48,978 --> 00:07:50,128
Un petit peu.

141
00:07:50,705 --> 00:07:52,817
D'accord, peut-�tre un petit peu.

142
00:07:52,942 --> 00:07:56,459
On faisait des mimes, et j'ignorais
comment faire deviner mon mot,

143
00:07:56,584 --> 00:07:59,434
et tout ce qui m'est venu
� l'esprit, c'est...

144
00:07:59,978 --> 00:08:03,281
- En un �clair, Cam a dit...
- <i>Casablanca</i>.

145
00:08:04,180 --> 00:08:06,680
- J'ai juste fait �a...
- <i>Casablanca</i>.

146
00:08:07,627 --> 00:08:08,777
<i>Casablanca</i>.

147
00:08:09,124 --> 00:08:11,123
On prend les couches et on s'en va ?

148
00:08:11,248 --> 00:08:13,544
D'accord, mais je passe d'abord
au rayon vins.

149
00:08:13,669 --> 00:08:16,244
Attends. Il y a un rayon vins ?

150
00:08:16,423 --> 00:08:20,373
- Un top. Juste apr�s le rayon pneus.
- Pas possible, ils ont pas...

151
00:08:20,596 --> 00:08:21,796
J'y crois pas.

152
00:08:23,906 --> 00:08:26,765
La d�chiqueteuse que je voulais.
Confettis et bandes.

153
00:08:31,128 --> 00:08:33,028
J'y crois pas, c'est g�nial.

154
00:08:38,917 --> 00:08:40,667
C'est quoi, cet endroit ?

155
00:08:43,765 --> 00:08:46,458
Ces cookies sentent trop bon.
C'est ta recette ?

156
00:08:46,976 --> 00:08:49,486
Non, je les ai juste mis
dans le four.

157
00:08:49,655 --> 00:08:51,613
Et tu les as pr�par�s avec amour ?

158
00:08:52,116 --> 00:08:53,066
C'est �a.

159
00:08:53,752 --> 00:08:55,635
Tu t'amuses bien, avec Luke ?

160
00:08:55,760 --> 00:08:58,037
Je sais pas.
Il veut pas sortir de son carton.

161
00:08:58,941 --> 00:09:02,405
Je vais rester pour passer
du temps avec ma s�ur.

162
00:09:02,708 --> 00:09:05,669
Oui, techniquement,
je suis ta demi-s�ur.

163
00:09:05,925 --> 00:09:08,368
Ma m�re aime pas
qu'on le dise comme �a,

164
00:09:08,493 --> 00:09:11,675
parce que �a veut dire "pas vrai",
alors qu'on est une vraie famille.

165
00:09:13,260 --> 00:09:15,310
Qu'est-ce qui va pas, Claire ?

166
00:09:17,006 --> 00:09:19,156
- Pardon ?
- Tu as l'air triste.

167
00:09:21,061 --> 00:09:23,963
C'est juste Alex.
Tu sais, des trucs de gamin.

168
00:09:24,088 --> 00:09:25,199
Ah, les gamins.

169
00:09:25,399 --> 00:09:27,899
M'en parle pas.
Y en a plein � l'�cole.

170
00:09:30,893 --> 00:09:34,155
Apr�s le d�jeuner,
on pourrait aller faire du shopping.

171
00:09:34,374 --> 00:09:37,867
- Je m'ach�terai pas de robe.
- Je m'en fiche, je suis pas ta m�re.

172
00:09:38,047 --> 00:09:40,996
- Je sais. T'es ma grand-m�re.
- Ta belle-grand-m�re.

173
00:09:42,425 --> 00:09:45,044
Bref, aujourd'hui,
vois-moi comme ton amie.

174
00:09:45,169 --> 00:09:48,120
Deux copines
qui s'amusent tout l'apr�s-midi.

175
00:09:49,165 --> 00:09:51,965
Tu fais quoi,
avec tes copines, d'habitude ?

176
00:09:52,371 --> 00:09:54,635
Je sais pas trop.
On sort. On discute,

177
00:09:55,022 --> 00:09:57,322
- on boit du vin.
- J'ai le droit ?

178
00:09:58,659 --> 00:09:59,859
Super, l'amie.

179
00:10:00,735 --> 00:10:03,027
Tu peux quand m�me me parler
de ce que tu veux.

180
00:10:03,152 --> 00:10:05,656
Les adultes disent toujours �a
sans le penser.

181
00:10:06,199 --> 00:10:09,463
Quand ma m�re dit que je peux
tout lui demander, c'est pas vrai.

182
00:10:09,588 --> 00:10:11,088
Elle flippe trop.

183
00:10:11,783 --> 00:10:13,133
Je flipperai pas.

184
00:10:13,697 --> 00:10:14,597
Balance.

185
00:10:17,430 --> 00:10:20,730
- T'as couch� avec combien d'hommes ?
- Huit. Suivante.

186
00:10:26,401 --> 00:10:27,751
Je peux essayer ?

187
00:10:30,520 --> 00:10:33,174
Faudrait que je m'en ach�te un.
J'adore les avions.

188
00:10:33,512 --> 00:10:36,384
Si ma vie avait �t� diff�rente,
j'aurais �t� pilote.

189
00:10:38,066 --> 00:10:40,920
Il se passerait quoi si vous �teigniez
et rallumiez la t�l�commande ?

190
00:10:41,045 --> 00:10:43,595
J'avoue,
t'aurais fait un super pilote.

191
00:10:44,415 --> 00:10:46,632
Vous pouvez faire un looping ?

192
00:10:46,827 --> 00:10:49,777
- Si je voulais.
- Trop bien ! Je peux essayer ?

193
00:10:53,904 --> 00:10:57,238
Je comprends que �a vous plaise.
C'est tellement paisible.

194
00:10:57,593 --> 00:11:01,368
C'est comme si le monde...
disparaissait autour de nous.

195
00:11:04,295 --> 00:11:06,684
- �a me rappelle un �t�...
- �coute-moi.

196
00:11:06,809 --> 00:11:10,353
Il y a un jeu, "le juste milieu".
Claire et Mitch y jouaient, petits.

197
00:11:10,478 --> 00:11:12,087
- �a te dit ?
- On fait comment ?

198
00:11:12,265 --> 00:11:14,215
Tu prends un de ces cerceaux,

199
00:11:14,379 --> 00:11:17,191
tu vas � l'autre bout du terrain,

200
00:11:18,004 --> 00:11:21,013
tu y restes et je fais passer
l'avion dans le cerceau.

201
00:11:21,440 --> 00:11:23,351
�a a l'air g�nial !

202
00:11:23,810 --> 00:11:26,010
Je pourrai le faire voler, apr�s ?

203
00:11:39,197 --> 00:11:40,323
C'est bon, l� ?

204
00:11:40,602 --> 00:11:42,335
Pas assez loin. Continue.

205
00:11:42,773 --> 00:11:45,071
- Je continue.
- �a sera jamais trop loin.

206
00:11:45,196 --> 00:11:47,094
D'accord. J'ai compris.

207
00:11:52,601 --> 00:11:54,543
- Et l� ?
- C'est bon !

208
00:11:55,108 --> 00:11:57,626
Ne bouge plus ! Reste immobile !

209
00:11:58,202 --> 00:11:59,365
C'est parti !

210
00:12:02,919 --> 00:12:04,063
J'adore �a !

211
00:12:04,349 --> 00:12:05,349
J'adore...

212
00:12:05,696 --> 00:12:06,496
�a !

213
00:12:15,909 --> 00:12:18,572
- Vous avez envoy� l'avion dans ma t�te.
- T'as d� bouger.

214
00:12:18,697 --> 00:12:20,712
- Je t'avais pr�venu.
- J'ai pas boug� !

215
00:12:20,837 --> 00:12:23,737
Tais-toi. Je veux v�rifier
que c'est pas cass�.

216
00:12:25,163 --> 00:12:29,167
{\pos(192,205)}Si on est en d�saccord, Alex et moi,
elle en fait toujours un affrontement.

217
00:12:30,045 --> 00:12:31,945
{\pos(192,205)}Une femme forte et ind�pendante.

218
00:12:32,070 --> 00:12:35,357
{\pos(192,205)}- �a me rappelle quelqu'un.
- J'ai jamais �t� hostile, moi.

219
00:12:35,482 --> 00:12:39,216
{\pos(192,205)}Cette histoire de robe...
J'ai jamais eu ce probl�me avec Haley.

220
00:12:39,639 --> 00:12:42,458
Peut-�tre qu'Alex veut pas
rivaliser avec sa s�ur.

221
00:12:42,583 --> 00:12:45,084
Elle essaie peut-�tre
de se faire sa propre identit�.

222
00:12:45,209 --> 00:12:48,687
- En robe, elle n'est plus elle-m�me.
- C'est l'espace d'un apr�s-midi.

223
00:12:48,812 --> 00:12:52,562
Jamais j'accepterais de renier
ma personnalit�, m�me un instant.

224
00:12:53,306 --> 00:12:56,042
On a parfois du mal � se dire
que tu n'as que 10 ans.

225
00:12:56,167 --> 00:12:57,667
10 ans trois quarts.

226
00:13:01,865 --> 00:13:03,949
- <i>C'est Mitchell. Besoin d'olives ?</i>
- D'olives ?

227
00:13:04,117 --> 00:13:05,700
- <i>Tr�s bonnes.</i>
- D'accord.

228
00:13:05,869 --> 00:13:06,919
�a marche.

229
00:13:07,473 --> 00:13:10,013
J'ai trouv� les couches.
Elles sont par l�.

230
00:13:10,160 --> 00:13:11,987
Devine ce que j'ai trouv�.

231
00:13:12,290 --> 00:13:15,582
Des cercueils. Ils vendent
du lait en poudre et des cercueils.

232
00:13:15,752 --> 00:13:18,881
On peut vraiment tout acheter,
de notre naissance � notre mort.

233
00:13:19,436 --> 00:13:22,217
Regarde ces couches.
Elles sont pas ch�res !

234
00:13:22,595 --> 00:13:25,223
Faudrait en prendre assez
pour un ou deux ans, non ?

235
00:13:25,348 --> 00:13:27,098
Et on les mettrait o� ?

236
00:13:27,937 --> 00:13:31,195
- Ils vendent des box.
- Tu veux un box � couches ?

237
00:13:31,363 --> 00:13:33,655
On est du genre � avoir
des box � couches ?

238
00:13:34,404 --> 00:13:37,650
- Prends deux autres paquets.
- S�rement pas. On s'en va.

239
00:13:37,818 --> 00:13:39,100
Allez. On a besoin de...

240
00:13:40,064 --> 00:13:42,691
Excusez-moi.
O� vous avez trouv� ce plateau ?

241
00:13:42,816 --> 00:13:44,692
- L�-bas.
- Va en chercher un.

242
00:13:44,817 --> 00:13:47,317
- S�rieusement ?
- Oui. Prends-en deux.

243
00:13:50,767 --> 00:13:52,526
Je vais au rayon compotes !

244
00:13:52,651 --> 00:13:54,940
Bref, apr�s les mimes,
on a pris un caf�,

245
00:13:55,065 --> 00:13:57,183
et j'ai appris qu'il aimait l'art,

246
00:13:57,308 --> 00:14:00,258
qu'il jouait du piano
et qu'il parlait fran�ais.

247
00:14:00,590 --> 00:14:01,440
<i>Un peu</i>.

248
00:14:01,596 --> 00:14:03,396
Je me suis tromp� sur lui.

249
00:14:03,964 --> 00:14:05,677
Je suis un peu comme Lidl.

250
00:14:05,802 --> 00:14:08,718
Je suis imposant, pas sophistiqu�,
et je vous d�fie de ne pas m'aimer.

251
00:14:08,843 --> 00:14:10,744
Moi, je suis plut�t comme...

252
00:14:12,242 --> 00:14:15,272
Elle s'appelait comment,
la boutique o� on est all�s, � Paris ?

253
00:14:15,397 --> 00:14:17,097
T'es vraiment trop snob.

254
00:14:20,054 --> 00:14:21,104
<i>Casablanca</i>.

255
00:14:23,238 --> 00:14:25,461
J'ai une petite fleur
sur la cheville

256
00:14:25,586 --> 00:14:27,836
et un tout petit papillon sur la...

257
00:14:28,432 --> 00:14:29,282
cuisse.

258
00:14:29,662 --> 00:14:30,776
� quel endroit ?

259
00:14:31,241 --> 00:14:32,241
Tr�s haut.

260
00:14:32,379 --> 00:14:35,749
Tu l'as fait pour �tre sexy ?
Non, tu l'as fait pour un mec ?

261
00:14:35,917 --> 00:14:38,952
- �a lui pla�t, � papy ?
- Non, non et oui.

262
00:14:39,754 --> 00:14:40,904
D�go�tant.

263
00:14:41,923 --> 00:14:43,773
Reparlons de tes piercings.

264
00:14:44,426 --> 00:14:48,058
J'ai r�pondu � assez de questions.
� toi de r�pondre.

265
00:14:48,655 --> 00:14:51,705
- � quoi ?
- Pourquoi tu veux pas mettre de robe ?

266
00:14:51,930 --> 00:14:55,070
Je veux pas ressembler � Haley
et � ses copines d�biles.

267
00:14:55,245 --> 00:14:57,608
Je mets des robes.
Je ressemble pas � Haley pour autant.

268
00:14:57,733 --> 00:15:01,076
Tu es Haley, mais avec...
quarante ans de plus.

269
00:15:01,401 --> 00:15:02,570
- Dix.
- Vingt.

270
00:15:02,763 --> 00:15:03,763
�a marche.

271
00:15:03,992 --> 00:15:07,531
- Alex, pourquoi tu r�agis comme �a ?
- J'ai pas besoin de bien m'habiller.

272
00:15:07,656 --> 00:15:09,950
Je suis pas aussi belle
qu'Haley et toi.

273
00:15:10,118 --> 00:15:12,851
- N'importe quoi ! Tu es tr�s belle !
- Non.

274
00:15:12,976 --> 00:15:15,757
Mais c'est pas grave.
Je suis intelligente.

275
00:15:16,923 --> 00:15:19,460
Le serveur sait pas
que t'es intelligente.

276
00:15:19,628 --> 00:15:20,428
Quoi ?

277
00:15:20,717 --> 00:15:23,493
- Il te sourit d�s qu'il vient par ici.
- Pas du tout !

278
00:15:23,618 --> 00:15:26,968
Pourquoi on est les seules
� avoir du pain, � ton avis ?

279
00:15:28,292 --> 00:15:30,062
�coute, un jour,

280
00:15:30,308 --> 00:15:32,873
tu voudras sortir
avec un beau gar�on.

281
00:15:33,240 --> 00:15:36,236
Ce jour-l�,
tu voudras te faire toute belle,

282
00:15:36,603 --> 00:15:38,395
et l�, tu mettras une robe.

283
00:15:40,813 --> 00:15:42,691
T'as d�j� embrass� une fille ?

284
00:15:43,103 --> 00:15:45,611
Ne parle pas
� ta grand-m�re sur ce ton.

285
00:15:45,779 --> 00:15:47,862
Si ma m�re �tait dure,
quand j'�tais petite ?

286
00:15:47,987 --> 00:15:50,091
Oui, mais tu sais quoi ?

287
00:15:50,216 --> 00:15:53,764
C'�tait la grande �poque
des f�ministes.

288
00:15:54,303 --> 00:15:57,303
- Elles comprenaient pas...
- <i>Hola.</i> Nous revoil�.

289
00:15:57,541 --> 00:16:01,235
- �a s'est bien pass� ?
- Super. Tu t'es amus�, avec Luke ?

290
00:16:01,504 --> 00:16:04,088
- Ben, pas...
- On a pass� un tr�s bon moment.

291
00:16:04,389 --> 00:16:06,998
Alex, j'ai r�fl�chi et...

292
00:16:08,148 --> 00:16:11,303
si tu veux pas mettre une robe,
c'est pas la fin du monde.

293
00:16:11,672 --> 00:16:12,672
C'est bon.

294
00:16:13,729 --> 00:16:15,179
J'en ai achet� une.

295
00:16:16,643 --> 00:16:20,431
- Tu vois comme elle me provoque ?
- On en parlera la semaine prochaine.

296
00:16:21,731 --> 00:16:24,882
Laissez passer.
On entre. Doucement.

297
00:16:25,357 --> 00:16:27,569
- Faut que mon visage se repose.
- Tu vas bien.

298
00:16:27,737 --> 00:16:28,972
Tu survivras.

299
00:16:29,138 --> 00:16:30,599
Oh, mon Dieu !

300
00:16:31,142 --> 00:16:33,409
- Papa !
- Petit accident. Rien de grave.

301
00:16:33,577 --> 00:16:35,686
- J'ai eu un accident d'avion.
- C'est arriv� comment ?

302
00:16:35,811 --> 00:16:38,079
On jouait au juste milieu,
il a boug�.

303
00:16:38,204 --> 00:16:40,869
Pas du tout.
Vous l'avez fait expr�s.

304
00:16:40,994 --> 00:16:44,039
C'est les m�docs qui parlent.
Il est un peu dans les vapes.

305
00:16:44,164 --> 00:16:46,191
- C'est grave ?
- L'aile est cass�e,

306
00:16:46,316 --> 00:16:48,600
- l'h�lice tordue, mais je...
- Je parlais de Phil.

307
00:16:49,021 --> 00:16:51,821
- J'ai eu un accident d'avion.
- Merci, papa.

308
00:16:52,053 --> 00:16:54,279
Je lui ai pas demand� de venir.
Il a insist�.

309
00:16:54,404 --> 00:16:57,724
- Il a pris un risque. C'est tout.
- Pareil qu'en lui roulant sur le pied ?

310
00:16:57,892 --> 00:16:59,946
- T'as fait �a ?
- C'�tait un accident.

311
00:17:00,071 --> 00:17:02,645
Tu l'as fait expr�s. On a vu
les traces de pneu sur la pelouse.

312
00:17:02,814 --> 00:17:04,764
Je venais d'arr�ter de fumer.

313
00:17:05,040 --> 00:17:07,390
- Tu vois, tu l'�nerves.
- De l'eau.

314
00:17:11,865 --> 00:17:12,715
�a va ?

315
00:17:12,861 --> 00:17:15,845
Gloria. Dieu merci, tu es l�.

316
00:17:16,034 --> 00:17:18,066
T'en fais pas. Je suis l� aussi.

317
00:17:18,191 --> 00:17:19,491
C'est qui, lui ?

318
00:17:20,662 --> 00:17:22,074
J'y suis pour rien.

319
00:17:22,199 --> 00:17:24,463
Je t'ai vu jouer � ce jeu
des millions de fois.

320
00:17:24,588 --> 00:17:26,681
- Tu te loupes jamais.
- C'est l'�ge.

321
00:17:26,806 --> 00:17:28,894
Tu veux pas admettre
que t'as jamais aim� Phil ?

322
00:17:29,019 --> 00:17:30,716
Pose-moi �a. T'es priv� de cookie.

323
00:17:30,893 --> 00:17:34,849
J'aime bien Phil, mais parfois,
il en fait trop, tu vois ?

324
00:17:34,974 --> 00:17:37,649
Et �a a tendance �... m'�nerver.

325
00:17:37,774 --> 00:17:40,853
Peut-�tre qu'il en fait trop
parce que toi, tu fais aucun effort.

326
00:17:40,978 --> 00:17:43,237
Tu sais quoi ?
Depuis 16 ans qu'on est mari�s,

327
00:17:43,405 --> 00:17:46,255
tu lui as d�j� dit
que tu l'appr�ciais ?

328
00:17:47,050 --> 00:17:50,921
- Pas exactement dans ces termes.
- En aucun terme du tout, papa.

329
00:17:51,046 --> 00:17:53,346
Tu crois que �a ne m'atteint pas ?

330
00:17:54,785 --> 00:17:56,974
T'as jamais dit � ton fils
que tu l'aimais ?

331
00:17:57,245 --> 00:17:59,335
On est pass�s � "aimer",
maintenant ?

332
00:17:59,460 --> 00:18:03,393
- Techniquement, c'est pas mon fils.
- Donc, t'aimes pas Manny ?

333
00:18:03,518 --> 00:18:06,267
Bien s�r que si.
Crois-moi, quand j'�tais dans ce parc,

334
00:18:06,392 --> 00:18:08,170
j'aurais voulu que Manny soit l�.

335
00:18:08,339 --> 00:18:11,689
Pour pouvoir lui envoyer
l'avion en pleine t�te ?

336
00:18:13,538 --> 00:18:16,238
Papa, merci d'avoir ramen� Phil.

337
00:18:16,721 --> 00:18:19,472
J'ai trouv� un bout d'aile
dans ses cheveux.

338
00:18:22,660 --> 00:18:25,812
- En fait, c'est un aileron.
- Comme si �a m'int�ressait.

339
00:18:27,556 --> 00:18:28,856
Au revoir, papa.

340
00:18:45,906 --> 00:18:47,856
T'es en porcelaine, ou quoi ?

341
00:18:50,461 --> 00:18:52,311
Je voulais juste m'excuser.

342
00:18:53,512 --> 00:18:56,047
Que je me sois loup�
ou que tu aies boug�...

343
00:18:56,172 --> 00:18:58,386
- J'ai pas boug�.
- Peu importe...

344
00:18:58,723 --> 00:19:01,090
Je veux juste que tu saches
que je t'appr�cie.

345
00:19:01,215 --> 00:19:02,165
S�rieux ?

346
00:19:04,185 --> 00:19:06,235
Vous appr�ciez quoi, chez moi ?

347
00:19:09,209 --> 00:19:10,609
T'es un mec sympa.

348
00:19:11,513 --> 00:19:13,794
Tu fais de gros efforts.

349
00:19:14,626 --> 00:19:15,676
C'est vrai.

350
00:19:16,921 --> 00:19:19,908
- T'es un bon p�re.
- Vous avez raison. Approchez.

351
00:19:20,325 --> 00:19:21,575
Venez par l�.

352
00:19:23,254 --> 00:19:24,754
Je suis si heureux.

353
00:19:27,519 --> 00:19:30,239
- Qu'est-ce qu'il y a ?
- Il y a eu un petit accident,

354
00:19:30,364 --> 00:19:32,814
mais Jay dit � Phil
combien il l'aime.

355
00:19:33,360 --> 00:19:35,048
C'est sympa pour Phil.

356
00:19:35,216 --> 00:19:37,665
Super, vu qu'il a jamais dit �a
� mon petit ami.

357
00:19:37,790 --> 00:19:39,625
- Laisse tomber.
- C'est pas normal.

358
00:19:39,750 --> 00:19:42,222
Mitch a raison.
Va avec eux, Cam. Allez.

359
00:19:42,433 --> 00:19:43,683
Tu rigoles, l�.

360
00:19:45,139 --> 00:19:46,495
Viens donc, Cam.

361
00:19:46,620 --> 00:19:48,670
- Un petit c�lin ?
- Approche.

362
00:19:49,781 --> 00:19:50,806
Dis-le !

363
00:19:50,931 --> 00:19:52,695
Je t'appr�cie aussi, Cam.

364
00:19:52,820 --> 00:19:55,027
- Vous appr�ciez quoi, chez lui ?
- Bon sang.

365
00:19:55,195 --> 00:19:59,145
- O� tu as eu cette cam�ra ?
- Dans le meilleur magasin du monde.

366
00:19:59,643 --> 00:20:00,743
Merci, papa.

367
00:20:01,239 --> 00:20:03,439
Manny, photo. Viens l�, toi aussi.

368
00:20:03,913 --> 00:20:05,563
Viens par l�, <i>mi amigo</i>.

369
00:20:05,869 --> 00:20:09,019
- Bougez plus. Dites "cheese" !
- Faut pas exag�rer.

370
00:20:09,158 --> 00:20:10,751
- Cheese !
- Photo !

371
00:20:11,210 --> 00:20:12,510
Cheese ! <i>Queso</i> !

372
00:20:14,103 --> 00:20:16,383
- C'est magnifique !
- Oh, bon sang !

373
00:20:16,508 --> 00:20:19,517
On dirait pas que ses bras pourraient
enlacer le monde entier ?

374
00:20:19,642 --> 00:20:21,792
Je crois que j'ai �cras� un truc.

375
00:20:23,275 --> 00:20:25,275
{\pos(192,220)}J'ai toujours voulu une fille.

376
00:20:25,683 --> 00:20:28,440
{\pos(192,220)}Pour lui mettre des robes,
la coiffer,

377
00:20:28,565 --> 00:20:31,015
{\pos(192,220)}m'occuper de ses ongles,
la maquiller.

378
00:20:33,079 --> 00:20:36,333
{\pos(192,220)}Personne ne le sait,
mais jusqu'� ce qu'il ait un an,

379
00:20:36,824 --> 00:20:40,828
j'ai d�guis� Manny en fille et j'ai dit
� tout le monde que c'�tait ma fille.

380
00:20:41,803 --> 00:20:45,653
Mais je l'ai pas fait souvent.
Je voulais pas que �a le rende fou.

381
00:20:45,926 --> 00:20:49,930
Quand il a trouv� les photos, j'ai dit
que c'�tait sa s�ur jumelle morte.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
