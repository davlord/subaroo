#!/usr/bin/env python3

'''
Subaroo, a subtitle file manipulation tool
Copyright (C) 2015 davlord

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Contact via https://github.com/davlord
'''

import argparse
import re
import datetime
import tempfile
import os
import shutil

SECOND_IN_MILLIS = 1000
MINUTE_IN_MILLIS = SECOND_IN_MILLIS*60
HOUR_IN_MILLIS = MINUTE_IN_MILLIS*60
TIME_PATTERN = re.compile(b'(\d{2}):(\d{2}):(\d{2})\\,(\d{3})')

def milliseconds_to_string(milliseconds):
	dt = datetime.datetime.utcfromtimestamp(milliseconds/1000)
	return dt.strftime('%H:%M:%S,') + str(milliseconds%1000).zfill(3)

def time_match_to_milliseconds(match):
	hours = int(match.group(1))
	minutes = int(match.group(2))
	seconds = int(match.group(3))
	milliseconds = int(match.group(4))
	return milliseconds + seconds*SECOND_IN_MILLIS + minutes*MINUTE_IN_MILLIS + hours*HOUR_IN_MILLIS

def process_line(line, modifier):
	match_iterator = TIME_PATTERN.finditer(line)	
	for match in match_iterator:
		# compute new time	
		old_time = time_match_to_milliseconds(match)
		new_time = modifier + old_time
		# search and replace time in line
		line_list = list(line)
		line_list[match.start():match.end()] = milliseconds_to_string(new_time).encode('ascii')
		line = bytes(line_list)
	return line

def main(time_modifier, subtitle_file):
	source_file = open(subtitle_file, 'rb')
	destination_file =  tempfile.NamedTemporaryFile(delete=False)
	for line in source_file:
		processed_line = process_line(line, time_modifier)
		destination_file.write(processed_line)
	source_file.close()
	destination_file.close()
	absolute_source_file = os.path.abspath(source_file.name);
	absolute_destination_file = os.path.abspath(destination_file.name);	
	shutil.move(absolute_destination_file, absolute_source_file)
	print('Subtitle updated:',source_file.name)
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("modifier")
	parser.add_argument("file")
	args = parser.parse_args()
	main(time_modifier=int(args.modifier), subtitle_file=args.file)
